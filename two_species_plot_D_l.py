# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 23:58:41 2020

@author: casmp
"""


import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import two_species_D_comp as comp
import os as os
from scipy import stats
import seaborn as sns

   
#Time variables   
dt = comp.dt
timelapse = comp.timelapse # how often to record   #before: 100
Tfinal = comp.Tfinal  #Before: 3000
n = comp.n  #time steps


#Spatial variables
L = comp.L #length of box
l_N = comp.l_N  #interaction range for species N
l_P = comp.l_P  #for P
D = comp.D # diffusion coefficient


#Initial number of cells for each species (N and P)   
N0 = comp.N0
P0 = comp.P0    

#Proliferation rates
b_N = comp.b_N  
b_P = comp.b_P


#Death rates
d_N = comp.d_N #for species N
d_P = comp.d_P  #for species P


#Coupling constans

# N
a_N = comp.a_N #with N
c_N = comp.c_N #with P

# P
a_P = comp.a_P #with P
c_P = comp.c_P  #with N

# a_x -> competition with its own species x
# c_x -> competition of x with the other species y

# Equations:
# dN / dt = b_N * N - (d_N + a_N * N  + c_N * P) * N
# dP / dt = b_P * P - (d_P + a_P * P + c_P * N) * P


# colourcycle palettes
dark = sns.color_palette("dark")
bright = sns.color_palette("bright")
colorblind = sns.color_palette("colorblind")
deep = sns.color_palette("deep")
pastel = sns.color_palette("pastel")
    



###############################################################################################################################################


def two_save_ensemble_data(D,l, repetitions = 25):  
    
    print('Entering ensemble')

    # EULER METHOD
    # Initialising variables
    t = 0
    Euler_dt = 0.05
    Euler_n = int(Tfinal/Euler_dt)
    history_cells_theo = []
    nextrecordtime = 0
    timelapse_Euler = comp.timelapse
    # Initial number of cells
    N_theo_now = N0
    P_theo_now = P0
    
    # Loop in time
    for i in range(Euler_n):
        N_theo_past = N_theo_now
        P_theo_past = P_theo_now
                
        # Euler method
        N_theo_now = N_theo_past*(1+Euler_dt*(b_N - d_N - a_N*N_theo_past - c_N*P_theo_past))
        P_theo_now = P_theo_past*(1+Euler_dt*(b_P - d_P - a_P*P_theo_past - c_P*N_theo_past))
                
        t = t + Euler_dt

        if t >= nextrecordtime:
            history_cells_theo.append([t,N_theo_now,P_theo_now])
            nextrecordtime += timelapse_Euler
            
    
    history_Euler = np.array(history_cells_theo)
    
    for rep in tqdm(range(repetitions)):
        
        Path1 ='Files/Two/History/history_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0,P0)  #history of cells data
        
        Path2 ='Files/Two/History/time_stride_D_{:.3f}_l_{:.3f}_rep{}_Tfinal{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal,N0,P0)  #time vector for history of cells
        
        Path3 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0)   # number of N cells at t*
        Path4 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0)   # number of P cells at t*
        
        Path5 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0)
        Path6 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0)
               
        print('repetition {}'.format(rep))
        print('os.path.exists(Path1)', os.path.exists(Path1))
        print('Path1', Path1)
           
        if ((os.path.exists(Path1) is False) or (os.path.exists(Path3) is False) or (os.path.exists(Path4) is False) or (os.path.exists(Path5) is False) or (os.path.exists(Path6) is False)):
            
            print('Creating new file')
            
            # Run experiment
            history,cellsN, cellsP = comp.run_cells_linear(D,l,l)
            
            # Collect data from experiment
            history_N = history[:,1]
            history_P = history[:,2]
            
            history_NP = np.hstack((history_N,history_P))
            
            Nstar = np.array([history_N[np.argmax(history_N)]])    # np.argmax gives the position of the max of an array
            Pstar = np.array([history_P[np.argmax(history_N)]])
            
            Napprox = np.array([history_N[np.argmax(history_Euler[:,1])]])
            Papprox = np.array([history_P[np.argmax(history_Euler[:,1])]])
            
            # Calculate stride to reduce length of history_ensemble
            stride = int(len(history_N)/100)
            
            if stride == 0:
                stride += 1
                
            # Save data for history_ensemble, Nstar, Pstar, Napprox, Papprox
            
            #Path1
            np.savetxt('Files/Two/History/history_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0,P0), history_NP[::stride])
            
            #Path3 and Path4
            np.savetxt('Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0), Nstar)
            np.savetxt('Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0), Pstar)
            
            #Path5 and Path6
            np.savetxt('Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0), Napprox)
            np.savetxt('Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal, N0, P0), Papprox)
            
            
            if os.path.exists(Path2) == False:
                time = history[:,0]
                time_stride = time[::stride]
                #Path2
                np.savetxt('Files/Two/History/time_stride_D_{:.3f}_l_{:.3f}_rep{}_Tfinal{}_N0_{}_P0_{}.dat'.format(D,l,rep,Tfinal,N0,P0), time_stride)
                



############################################################################################################################################################################


def two_plot_D_star(fixedl,minD, maxD):
    
    list_final=[]
    N_aver=[]
    P_aver=[]
    Nstar_vector_hist = []
    Pstar_vector_hist = []
    
    for i, D in enumerate(np.geomspace(minD,maxD,11)):
        
        
        Path1 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,0,Tfinal, N0, P0)   # number of N cells at t*
        Path2 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,0,Tfinal, N0, P0)   # number of P cells at t*
        
      
        # Checking that we have a file for those values of D and l
        if os.path.exists(Path1) == True and os.path.exists(Path2) == True:
            print('Reading file',Path1)
            print('Reading file',Path2)
            
            reps = 0
            finished_search = False
            
            Nstar_vector = np.ones(0)
            Pstar_vector = np.ones(0)
            
            while(finished_search is False):
                print('Loading repeat', reps)
                
                path_try_1 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0)
                path_try_2 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0)
                
                
                if (os.path.exists(path_try_1)==True) and (os.path.exists(path_try_2)==True) and (reps<12):
                    
                    Nstar_vector = np.hstack((Nstar_vector,np.loadtxt('Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0))))    
                    Pstar_vector = np.hstack((Pstar_vector,np.loadtxt('Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0))))
                    
                    reps += 1
                    
                else:
                    finished_search = True
        else:
            print('Files ', Path1, 'and', Path2, 'not found!')
                        
                
        # Calculates average Nstar for each D
        
        average_Nstar = np.mean(Nstar_vector)
        std_Nstar = np.std(Nstar_vector) #standard deviation of Nfinal
        sem_Nstar = stats.sem(Nstar_vector)  #standard error of the mean
        N_aver.append(average_Nstar)
        Nstar_vector_hist = np.hstack((Nstar_vector_hist, Nstar_vector))
        
        average_Pstar = np.mean(Pstar_vector)
        std_Pstar = np.std(Pstar_vector) #standard deviation of Nfinal
        sem_Pstar = stats.sem(Pstar_vector)  #standard error of the mean
        P_aver.append(average_Pstar)
        Pstar_vector_hist = np.hstack((Pstar_vector_hist, Pstar_vector))
        
            
        list_final.append({'D':D,'Nstar':Nstar_vector, 'Pstar':Pstar_vector,
                           'Nstar_aver':average_Nstar, 'Pstar_aver': average_Pstar,
                           'Nstar_std':std_Nstar,'Pstar_std':std_Pstar,
                           'Nstar_sem':sem_Nstar, 'Pstar_sem':sem_Pstar})
            
    print('list_final is', list_final)
    
    
    plt.figure()
    plt.tight_layout()
    
    D_array = []
    Nstar_aver_array = []
    Pstar_aver_array = []
    Nstar_std_array = []
    Pstar_std_array = []
    Nstar_sem_array = []
    Pstar_sem_array = []
    
    
    
    for element in list_final:
        D_array.append(element['D'])
        Nstar_aver_array.append(element['Nstar_aver'])
        Pstar_aver_array.append(element['Pstar_aver'])
        Nstar_std_array.append(element['Nstar_std'])
        Pstar_std_array.append(element['Pstar_std'])
        Nstar_sem_array.append(element['Nstar_sem'])
        Pstar_sem_array.append(element['Pstar_sem'])        
    
    D_array = np.array(D_array)
    Nstar_aver_array = np.array(Nstar_aver_array)
    Pstar_aver_array = np.array(Pstar_aver_array)
    Nstar_std_array = np.array(Nstar_std_array)
    Pstar_std_array = np.array(Pstar_std_array)
    Nstar_sem_array = np.array(Nstar_sem_array)
    Pstar_sem_array = np.array(Pstar_sem_array)
    
    
    # Error bars (standard deviation)
    plt.errorbar(D_array,Nstar_aver_array,Nstar_std_array, capsize=3, alpha = 0.3, ecolor= pastel[0])
    plt.errorbar(D_array,Pstar_aver_array,Pstar_std_array, capsize=3, alpha = 0.3, ecolor= pastel[2])

    
    # Theoretical max N and corresponding value for P, varying D
    plt.plot(D_array, Nstar_aver_array, marker='.', color=bright[0], alpha = 1, label='Stochastic values of $N (t_{det})$')
    plt.plot(D_array, Pstar_aver_array, marker='.', color=bright[2], alpha = 1, label='Stochastic values of $P (t_{det})$')
    
    
    # Standard error of the mean
    plt.fill_between(D_array, Nstar_aver_array - Nstar_sem_array, Nstar_aver_array + Nstar_sem_array,  color = pastel[0] , alpha = 0.75)
    plt.fill_between(D_array, Pstar_aver_array - Pstar_sem_array, Pstar_aver_array + Pstar_sem_array,  color = pastel[2] , alpha = 0.75)
    
    # Labelling
    plt.title('$\\ell$={}'.format(fixedl))
    plt.xlabel('Diffusion $D$')
    plt.ylabel('$\\langle N(t_{det})\\rangle$ and $\\langle P(t_{det})\\rangle$')
    
    ax = plt.gca()
#    xleft, xright = ax.get_xlim()
#    yleft, yright = ax.get_ylim()
#    aspectratio = 1
#    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    
    ax.set_xscale('log')
    
    plt.legend(loc='upper right')
    
    plt.savefig('EULER_DET_with_l={}.pdf'.format(fixedl), dpi=350)
    plt.show()
    
    
    
###########################################################################################################################################################################################
      
    

def two_plot_l_star(fixedD,minl, maxl):
    
    list_final=[]
    N_aver=[]
    P_aver=[]
    Nstar_vector_hist = []
    Pstar_vector_hist = []
    
    
    for i, l in enumerate(np.linspace(minl,maxl, 11, endpoint=True)):
        
        Path1 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,0,Tfinal, N0, P0)   # number of N cells at t*
        Path2 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,0,Tfinal, N0, P0)   # number of P cells at t*
        
        
        # Checking that we have a file for those values of D and l
        if os.path.exists(Path1) == True and os.path.exists(Path2) == True:
            print('Reading file',Path1)
            print('Reading file',Path2)
            
            reps = 0
            finished_search = False
            
            Nstar_vector = np.ones(0)
            Pstar_vector = np.ones(0)
            
            while(finished_search is False):
                print('Loading repeat', reps)
                
                path_try_1 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0)
                path_try_2 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0)
                
                
                if (os.path.exists(path_try_1)==True) and (os.path.exists(path_try_2)==True) and (reps<12):
                    
                    Nstar_vector = np.hstack((Nstar_vector,np.loadtxt('Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0))))    
                    Pstar_vector = np.hstack((Pstar_vector,np.loadtxt('Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0))))
                    
                    reps += 1
                    
                else:
                    finished_search = True
        else:
            print('Files ', Path1,'and', Path2, 'not found!')
                        
          

        # Calculates average Nfinal for each D
        
        average_Nstar = np.mean(Nstar_vector)
        std_Nstar = np.std(Nstar_vector) #standard deviation of Nfinal
        sem_Nstar = stats.sem(Nstar_vector)  #standard error of the mean
        N_aver.append(average_Nstar)
        Nstar_vector_hist = np.hstack((Nstar_vector_hist, Nstar_vector))
        
        average_Pstar = np.mean(Pstar_vector)
        std_Pstar = np.std(Pstar_vector) #standard deviation of Nfinal
        sem_Pstar = stats.sem(Pstar_vector)  #standard error of the mean
        P_aver.append(average_Pstar)
        Pstar_vector_hist = np.hstack((Pstar_vector_hist, Pstar_vector))
        
            
        list_final.append({'l':l,'Nstar':Nstar_vector, 'Pstar':Pstar_vector,
                           'Nstar_aver':average_Nstar, 'Pstar_aver': average_Pstar,
                           'Nstar_std':std_Nstar,'Pstar_std':std_Pstar,
                           'Nstar_sem':sem_Nstar, 'Pstar_sem':sem_Pstar})
            
    print('list_final is', list_final)              
                
    
    plt.figure()
#    plt.set_aspect('equal', 'box')
    plt.tight_layout()
    
    l_array = []
    Nstar_aver_array = []
    Pstar_aver_array = []
    Nstar_std_array = []
    Pstar_std_array = []
    Nstar_sem_array = []
    Pstar_sem_array = []
    
    
    
    for element in list_final:
        l_array.append(element['l'])
        Nstar_aver_array.append(element['Nstar_aver'])
        Pstar_aver_array.append(element['Pstar_aver'])
        Nstar_std_array.append(element['Nstar_std'])
        Pstar_std_array.append(element['Pstar_std'])
        Nstar_sem_array.append(element['Nstar_sem'])
        Pstar_sem_array.append(element['Pstar_sem'])        
    
    l_array = np.array(l_array)
    Nstar_aver_array = np.array(Nstar_aver_array)
    Pstar_aver_array = np.array(Pstar_aver_array)
    Nstar_std_array = np.array(Nstar_std_array)
    Pstar_std_array = np.array(Pstar_std_array)
    Nstar_sem_array = np.array(Nstar_sem_array)
    Pstar_sem_array = np.array(Pstar_sem_array)
    
    
    
    plt.errorbar(l_array,Nstar_aver_array,Nstar_std_array, capsize=3, alpha = 0.3, ecolor= pastel[0])
    plt.errorbar(l_array,Pstar_aver_array,Pstar_std_array, capsize=3, alpha = 0.3, ecolor= pastel[2])
        
    
    plt.plot(l_array, Nstar_aver_array, marker='.', color=bright[0], alpha = 1, label='Stochastic values of $N (t^{*})$')
    
    plt.plot(l_array, Pstar_aver_array, marker='.', color=bright[2], alpha = 1, label='Stochastic values of $P (t^{*})$')
    
    plt.fill_between(l_array, Nstar_aver_array - Nstar_sem_array, Nstar_aver_array + Nstar_sem_array,  color = pastel[0] , alpha = 0.75)
    plt.fill_between(l_array, Pstar_aver_array - Pstar_sem_array, Pstar_aver_array + Pstar_sem_array,  color = pastel[2] , alpha = 0.75)
    
    
    plt.title('$D$={}'.format(fixedD))
    plt.xlabel('Interaction range $\\ell$')
    plt.ylabel('$\\langle N(t_{det})\\rangle$ and $\\langle P(t_{det})\\rangle$')
    
    
#    ax = plt.gca()
#    xleft, xright = ax.get_xlim()
#    yleft, yright = ax.get_ylim()
#    aspectratio = 1
#    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
#    ax.set_xscale('log')
    
    plt.legend(loc='upper right')
    
    plt.savefig('EULER_DET_with_D={:.3f}.pdf'.format(fixedD), dpi=350)
    plt.show()

        
  
#######################################################################################################################################################################

def two_plot_D_approx(fixedl,minD, maxD):
    
    list_final=[]
    N_aver=[]
    P_aver=[]
    Napprox_vector_hist = []
    Papprox_vector_hist = []
    
    for i, D in enumerate(np.geomspace(minD,maxD,11)):
        
        Path1 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,0,Tfinal, N0, P0)   # number of N cells at t*
        Path2 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,0,Tfinal, N0, P0)   # number of P cells at t*
        
      
        # Checking that we have a file for those values of D and l
        if os.path.exists(Path1) == True and os.path.exists(Path2) == True:
            print('Reading file',Path1)
            print('Reading file',Path2)
            
            reps = 0
            finished_search = False
            
            Napprox_vector = np.ones(0)
            Papprox_vector = np.ones(0)
            
            while(finished_search is False):
                print('Loading repeat', reps)
                
                path_try_1 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0)
                path_try_2 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0)
                
                
                if (os.path.exists(path_try_1)==True) and (os.path.exists(path_try_2)==True) and (reps<12):
                    
                    Napprox_vector = np.hstack((Napprox_vector,np.loadtxt('Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0))))    
                    Papprox_vector = np.hstack((Papprox_vector,np.loadtxt('Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, P0))))
                    
                    reps += 1
                    
                else:
                    finished_search = True
        else:
            print('Files ', Path1, 'and', Path2, 'not found!')
                        
                
        # Calculates average Nfinal for each D
        
        average_Napprox = np.mean(Napprox_vector)
        std_Napprox = np.std(Napprox_vector) #standard deviation of Nfinal
        sem_Napprox = stats.sem(Napprox_vector)  #standard error of the mean
        N_aver.append(average_Napprox)
        Napprox_vector_hist = np.hstack((Napprox_vector_hist, Napprox_vector))
        
        average_Papprox = np.mean(Papprox_vector)
        std_Papprox = np.std(Papprox_vector) #standard deviation of Nfinal
        sem_Papprox = stats.sem(Papprox_vector)  #standard error of the mean
        P_aver.append(average_Papprox)
        Papprox_vector_hist = np.hstack((Papprox_vector_hist, Papprox_vector))
        
            
        list_final.append({'D':D,'Napprox':Napprox_vector, 'Papprox':Papprox_vector,
                           'Napprox_aver':average_Napprox, 'Papprox_aver': average_Papprox,
                           'Napprox_std':std_Napprox,'Papprox_std':std_Papprox,
                           'Napprox_sem':sem_Napprox, 'Papprox_sem':sem_Papprox})
            
    print('list_final is', list_final)
    
    
    plt.figure()
    plt.tight_layout()
    
    D_array = []
    Napprox_aver_array = []
    Papprox_aver_array = []
    Napprox_std_array = []
    Papprox_std_array = []
    Napprox_sem_array = []
    Papprox_sem_array = []


    for element in list_final:
        D_array.append(element['D'])
        Napprox_aver_array.append(element['Napprox_aver'])
        Papprox_aver_array.append(element['Papprox_aver'])
        Napprox_std_array.append(element['Napprox_std'])
        Papprox_std_array.append(element['Papprox_std'])
        Napprox_sem_array.append(element['Napprox_sem'])
        Papprox_sem_array.append(element['Papprox_sem'])   
    
    D_array = np.array(D_array)
    Napprox_aver_array = np.array(Napprox_aver_array)
    Papprox_aver_array = np.array(Papprox_aver_array)
    Napprox_std_array = np.array(Napprox_std_array)
    Papprox_std_array = np.array(Papprox_std_array)
    Napprox_sem_array = np.array(Napprox_sem_array)
    Papprox_sem_array = np.array(Papprox_sem_array)

    
    # Error bars (standard deviation)
    plt.errorbar(D_array,Napprox_aver_array, Napprox_std_array, capsize=3, alpha = 0.3, ecolor= pastel[0])
    plt.errorbar(D_array,Papprox_aver_array, Papprox_std_array, capsize=3, alpha = 0.3, ecolor= pastel[2])
    
    # Experimental max N and corresponding value for P, varying D
    plt.plot(D_array, Napprox_aver_array, marker='.', color=bright[0], alpha = 1, label= 'Stochastic values of $N (t^{*})$')
    plt.plot(D_array, Papprox_aver_array, marker='.', color=bright[2], alpha = 1, label='Stochastic values of $P(t^{*})$')
  
    
    # Standard error of the mean
    plt.fill_between(D_array, Napprox_aver_array - Napprox_sem_array, Napprox_aver_array + Napprox_sem_array,  color = pastel[0] , alpha = 0.75)
    plt.fill_between(D_array, Papprox_aver_array - Papprox_sem_array, Papprox_aver_array + Papprox_sem_array,  color = pastel[2] , alpha = 0.75)
    
    # Labelling
    plt.title('$\\ell$={}'.format(fixedl))
    plt.xlabel('Diffusion $D$')
    plt.ylabel('$\\langle N(t^{*})\\rangle$ and $\\langle P(t^{*})\\rangle$)')
    
    ax = plt.gca()
#    xleft, xright = ax.get_xlim()
#    yleft, yright = ax.get_ylim()
#    aspectratio = 1
#    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    
    ax.set_xscale('log')
    
    plt.legend(loc='upper right')
    
    
    plt.savefig('APPROX_STAR_l_={}.pdf'.format(fixedl), dpi=350)
    plt.show()
    
    
    
#######################################################################################################################################################################
    

def two_plot_l_approx(fixedD,minl, maxl):
    
    list_final=[]
    N_aver=[]
    P_aver=[]
    Napprox_vector_hist = []
    Papprox_vector_hist = []
    
    
    for i, l in enumerate(np.linspace(minl,maxl, 11, endpoint=True)):
        
        Path1 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,0,Tfinal, N0, P0)   # number of N cells at t*
        Path2 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,0,Tfinal, N0, P0)   # number of P cells at t*
        
        
        # Checking that we have a file for those values of D and l
        if os.path.exists(Path1) == True and os.path.exists(Path2) == True:
            print('Reading file',Path1)
            print('Reading file',Path2)
            
            reps = 0
            finished_search = False
            
            Napprox_vector = np.ones(0)
            Papprox_vector = np.ones(0)
            
            while(finished_search is False):
                print('Loading repeat', reps)
                
                path_try_1 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0)
                path_try_2 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0)
                
                
                if (os.path.exists(path_try_1)==True) and (os.path.exists(path_try_2)==True) and (reps<12):
                    
                    Napprox_vector = np.hstack((Napprox_vector,np.loadtxt('Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0))))    
                    Papprox_vector = np.hstack((Papprox_vector,np.loadtxt('Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, P0))))
                    
                    reps += 1
                    
                else:
                    finished_search = True
        else:
            print('Files ', Path1,'and', Path2, 'not found!')
                        
          

        # Calculates average Nfinal for each D
        
        average_Napprox = np.mean(Napprox_vector)
        std_Napprox = np.std(Napprox_vector) #standard deviation of Nfinal
        sem_Napprox = stats.sem(Napprox_vector)  #standard error of the mean
        N_aver.append(average_Napprox)
        Napprox_vector_hist = np.hstack((Napprox_vector_hist, Napprox_vector))
        
        average_Papprox = np.mean(Papprox_vector)
        std_Papprox = np.std(Papprox_vector) #standard deviation of Nfinal
        sem_Papprox = stats.sem(Papprox_vector)  #standard error of the mean
        P_aver.append(average_Papprox)
        Papprox_vector_hist = np.hstack((Papprox_vector_hist, Papprox_vector))
        
            
        list_final.append({'l':l,'Napprox':Napprox_vector, 'Papprox':Papprox_vector,
                           'Napprox_aver':average_Napprox, 'Papprox_aver': average_Papprox,
                           'Napprox_std':std_Napprox,'Papprox_std':std_Papprox,
                           'Napprox_sem':sem_Napprox, 'Papprox_sem':sem_Papprox})
            
    print('list_final is', list_final)              
                
    
    plt.figure()
    plt.tight_layout()
    
    
    l_array = []
    Napprox_aver_array = []
    Papprox_aver_array = []
    Napprox_std_array = []
    Papprox_std_array = []
    Napprox_sem_array = []
    Papprox_sem_array = []


    for element in list_final:
        l_array.append(element['l'])
        Napprox_aver_array.append(element['Napprox_aver'])
        Papprox_aver_array.append(element['Papprox_aver'])
        Napprox_std_array.append(element['Napprox_std'])
        Papprox_std_array.append(element['Papprox_std'])
        Napprox_sem_array.append(element['Napprox_sem'])
        Papprox_sem_array.append(element['Papprox_sem'])   
    
    l_array = np.array(l_array)
    Napprox_aver_array = np.array(Napprox_aver_array)
    Papprox_aver_array = np.array(Papprox_aver_array)
    Napprox_std_array = np.array(Napprox_std_array)
    Papprox_std_array = np.array(Papprox_std_array)
    Napprox_sem_array = np.array(Napprox_sem_array)
    Papprox_sem_array = np.array(Papprox_sem_array)
    
        
    # Error bars (standard deviation)
    plt.errorbar(l_array,Napprox_aver_array, Napprox_std_array, capsize=3, alpha = 0.3, ecolor= pastel[0])
    plt.errorbar(l_array,Papprox_aver_array, Papprox_std_array, capsize=3, alpha = 0.3, ecolor= pastel[2])
    
    # Experimental max N and corresponding value for P, varying D
    plt.plot(l_array, Napprox_aver_array, marker='.', color=bright[0], alpha = 1, label= 'Stochastic values of $N (t^{*})$')
    plt.plot(l_array, Papprox_aver_array, marker='.', color=bright[2], alpha = 1, label='Stochastic values of $P(t^{*})$')
  
    
    # Standard error of the mean
    plt.fill_between(l_array, Napprox_aver_array - Napprox_sem_array, Napprox_aver_array + Napprox_sem_array,  color = pastel[0] , alpha = 0.75)
    plt.fill_between(l_array, Papprox_aver_array - Papprox_sem_array, Papprox_aver_array + Papprox_sem_array,  color = pastel[2] , alpha = 0.75)
    
    # Labelling
    plt.title('$D$={}'.format(fixedD))
    plt.xlabel('Interaction range $\\ell$')
    plt.ylabel('$\\langle N(t^{*})\\rangle$ and $\\langle P(t^{*})\\rangle$)')
    
#    ax = plt.gca()
##    xleft, xright = ax.get_xlim()
##    yleft, yright = ax.get_ylim()
##    aspectratio = 1
##    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
#    
#    ax.set_xscale('log')
    
    plt.legend(loc='upper right')
    
    
    plt.savefig('APPROX_STAR_$D$={:.3f}.pdf'.format(fixedD), dpi=350)
    plt.show()



    
#######################################################################################################################################################################
    
    
def histogram_approx(D,l):
        
    Path1 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,0,Tfinal, N0, P0)   # number of N cells at t*
    Path2 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,0,Tfinal, N0, P0) 
    
    if os.path.exists(Path1) == True and os.path.exists(Path2) == True:
        print('Reading file',Path1)
        print('Reading file',Path2)
        
        reps = 0
        finished_search = False
                
        Napprox_vector = np.ones(0)
        Papprox_vector = np.ones(0)
                
        while(finished_search is False):
            print('Loading repeat', reps)
            
            path_try_1 ='Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0)
            path_try_2 ='Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0)
            
            
            if os.path.exists(path_try_1)==True and os.path.exists(path_try_2)==True:
                
                Napprox_vector = np.hstack((Napprox_vector,np.loadtxt('Files/Two/Napprox/Napprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0))))    
                Papprox_vector = np.hstack((Papprox_vector,np.loadtxt('Files/Two/Papprox/Papprox_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0))))
                
                reps += 1
                
            else:
                finished_search = True
    else:
        print('Files ', Path1,'and', Path2, 'not found!')
                            
              

            
    average_Napprox = np.mean(Napprox_vector)
    std_Napprox = np.std(Napprox_vector) #standard deviation of Nfinal
    sem_Napprox = stats.sem(Napprox_vector)  #standard error of the mean

    average_Papprox = np.mean(Papprox_vector)
    std_Papprox = np.std(Papprox_vector) #standard deviation of Nfinal
    sem_Papprox = stats.sem(Papprox_vector)  #standard error of the mean
        
        
    plt.figure()
    plt.tight_layout()
        
    plt.hist(Napprox_vector, color=bright[0], bins=20, alpha=0.6, label='$N(t^{*})$')
    plt.hist(Papprox_vector, color=bright[2], bins=20, alpha=0.6, label= '$P(t^{*})$' )
        
    plt.title('$D = {}$ and $\\ell = {}$'.format(D,l))
    plt.xlabel('$N(t^{*})$ and $P(t^{*})$')
    plt.ylabel('Counts')
    plt.legend('upper right')
    #    plt.legend()
    ##   print('Nfinal_vector', Nfinal_vector_hist)
    ax = plt.gca()
    xleft, xright = ax.get_xlim()
    yleft, yright = ax.get_ylim()
    aspectratio = 1
    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    plt.savefig('Graphs\\Two\\APPROX_histogram_D={:.3f}_l={:.3f}_averN_={:.2f}_averP_={:.2f}_stdN={:.2f}_stdP={:.2f}.pdf'.format(D,l, average_Napprox, average_Papprox,  std_Napprox, std_Papprox), dpi=350)
    plt.show()
        


####################################################################################################################################################################################################################################
    
    
    
def histogram_Euler(D,l):
        
    Path1 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,0,Tfinal, N0, P0)   # number of N cells at t*
    Path2 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,0,Tfinal, N0, P0) 
    
    if os.path.exists(Path1) == True and os.path.exists(Path2) == True:
        print('Reading file',Path1)
        print('Reading file',Path2)
        
        reps = 0
        finished_search = False
                
        Nstar_vector = np.ones(0)
        Pstar_vector = np.ones(0)
                
        while(finished_search is False):
            print('Loading repeat', reps)
            
            path_try_1 ='Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0)
            path_try_2 ='Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0)
            
            
            if os.path.exists(path_try_1)==True and os.path.exists(path_try_2)==True:
                
                Nstar_vector = np.hstack((Nstar_vector,np.loadtxt('Files/Two/Nstar/Nstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0))))    
                Pstar_vector = np.hstack((Pstar_vector,np.loadtxt('Files/Two/Pstar/Pstar_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_P0_{}.dat'.format(D,l,reps,Tfinal, N0, P0))))
                
                reps += 1
                
            else:
                finished_search = True
    else:
        print('Files ', Path1,'and', Path2, 'not found!')
                            
              
            
    average_Nstar = np.mean(Nstar_vector)
    std_Nstar = np.std(Nstar_vector) #standard deviation of Nfinal
    sem_Nstar = stats.sem(Nstar_vector)  #standard error of the mean

    average_Pstar = np.mean(Pstar_vector)
    std_Pstar = np.std(Pstar_vector) #standard deviation of Nfinal
    sem_Pstar = stats.sem(Pstar_vector)  #standard error of the mean
        
        
    plt.figure()
    plt.tight_layout()
        
    plt.hist(Nstar_vector, color=bright[0], bins=20, alpha=0.6, label='$N(t_{det})$')
    plt.hist(Pstar_vector, color=bright[2], bins=20, alpha=0.6, label='$P(t_{det})$')
        
    plt.title('$D = {}$ and $\\ell = {}$'.format(D,l))
    plt.xlabel('$N(t_{det})$ and $P(t_{det})}$')
    plt.ylabel('Counts')
    plt.legend('upper right')
    #    plt.legend()
    ##   print('Nfinal_vector', Nfinal_vector_hist)
    ax = plt.gca()
    xleft, xright = ax.get_xlim()
    yleft, yright = ax.get_ylim()
    aspectratio = 1
    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    plt.savefig('Graphs\\Two\\STAR_histogram_D={:.3f}_l={:.3f}_averN_={:.2f}_averP_={:.2f}_stdN={:.2f}_stdP={:.2f}.pdf'.format(D,l, average_Nstar, average_Pstar,  std_Nstar, std_Pstar), dpi=350)
    plt.show()

    

#####################################################################################################################################################################################################################
    
       
    
# Saving the data needed for the plots    

for i in tqdm(np.geomspace(0.001,100.000,11)):    #geometrical series spacing / log scale spacing
    two_save_ensemble_data(i,0.500)
    two_save_ensemble_data(i,25.000)


for j in tqdm(np.linspace(0.500,25.000,11, endpoint=True)):
    two_save_ensemble_data(0.000,j)
    two_save_ensemble_data(0.001,j)
    two_save_ensemble_data(0.010,j)
    two_save_ensemble_data(1.000,j)
    two_save_ensemble_data(10.000,j)
    two_save_ensemble_data(100.000,j)
    

# Saving the data needed for the histograms

two_save_ensemble_data(0.001, 0.500, 250)
two_save_ensemble_data(10.000, 0.500, 250)

two_save_ensemble_data(0.001, 25.000, 250)
two_save_ensemble_data(10.000, 25.000, 250)


## Plots fixed D

# Max experimental N and respective P
two_plot_l_approx(0.000, 0.500, 25.000)
two_plot_l_approx(0.001, 0.500, 25.000)
two_plot_l_approx(1.000, 0.500, 25.000)
two_plot_l_approx(10.000, 0.500, 25.000) 

# Max theoretical N and respective P
two_plot_l_star(0.000, 0.500, 25.000)
two_plot_l_star(0.001, 0.500, 25.000) 
two_plot_l_star(1.000, 0.500, 25.000) 
two_plot_l_star(10.000, 0.500, 25.000)



## Plots fixed l

## Max exprimental N and respective P
two_plot_D_approx(0.500,0.001,100.000)
two_plot_D_approx(25.000,0.001,100.000)


## Max theoretical N and respective P
two_plot_D_star(0.500,0.001,100.000)
two_plot_D_star(25.000,0.001,100.000)


## Plotting and saving the histograms

## Max exprimental N and respective P
histogram_approx(0.001, 0.500)
histogram_approx(10.000, 0.500)
histogram_approx(0.001, 25.000)
histogram_approx(10.000, 25.000)


## Max theoretical N and respective P
histogram_Euler(0.001, 0.500)
histogram_Euler(10.000, 0.500)
histogram_Euler(0.001, 25.000)
histogram_Euler(10.000, 25.000)





