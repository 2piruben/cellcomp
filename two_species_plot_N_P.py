# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 14:03:52 2020

@author: casmp
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 15:14:20 2019

@author: casmp
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import two_species_cells_comp as two
import seaborn as sns


#This script contains the plots regarding two species cell competition

#Time parameters
dt = two.dt
timelapse = two.timelapse # how often to record   #before: 100
Tfinal = two.Tfinal  #Before: 3000
n = two.n  #time steps


#Spatial variables
L = two.L #length of box
l_N = two.l_N  #interaction range for species N
l_P = two.l_P  #for P
D = two.D # diffusion coefficient


#Initial number of cells for each species (N and P)   
N0 = two.N0
P0 = two.P0
    

#Proliferation rates
b_N = two.b_N 
b_P = two.b_P


#Death rates
d_N = two.d_N  #for species N
d_P = two.d_P  #for species P


#Coupling constans

# N
a_N = two.a_N  #with N
#c_N = two.c_N  #with P
c_N = two.c_N


# P
a_P = two.a_P  #with P
#c_P = two.c_P  #with N
c_P= two.c_P
# two.C_P = 0
# a_x -> competition with its own species x
# c_x -> competition of x with the other species y

# Equations:
# dN / dt = b_N * N - (d_N + a_N * N  + c_N * P) * N
# dP / dt = b_P * P - (d_P + a_P * P + c_P * N) * P

#run_cells_linear(D,l_N, l_P)
#    return np.array(history_cells), cellarrayN, cellarrayP

# colourcycle palettes
dark = sns.color_palette("dark")
bright = sns.color_palette("bright")
colorblind = sns.color_palette("colorblind")
deep = sns.color_palette("deep")
pastel = sns.color_palette("pastel")



def run_ensemble_two_species(hold = True, colorid = 0, colorid_P=2):
    repetitions = 50
    history_ensemble_N = np.ones(0) # empty numpy array
    history_ensemble_P = np.ones(0) # empty numpy array
    for rep in tqdm(range(repetitions)):
        history, cellsN,cellsP = two.run_cells_linear(D,l_N,l_P)
        if rep==0:
            time = history[:,0] 
            print('simulation time', time)
            history_ensemble_N = history[:,1]
            history_ensemble_P = history[:,2]
            
            # EULER METHOD
            # Initialising variables
            t = 0
            Euler_dt = 0.01
            Euler_n = int(Tfinal/Euler_dt)
            history_cells_theo = []
#            N_theo = np.zeros(Euler_n)  #empty array for Euler method
#            P_theo = np.zeros(Euler_n)
            nextrecordtime = 0
            timelapse_Euler = 1
            # Initial number of cells
            N_theo_now = N0
            P_theo_now = P0
            
            # Loop in time
            for i in range(Euler_n):
                N_theo_past = N_theo_now
                P_theo_past = P_theo_now
                
                # Euler method
                N_theo_now = N_theo_past*(1+Euler_dt*(b_N - d_N - a_N*N_theo_past - c_N*P_theo_past))
                P_theo_now = P_theo_past*(1+Euler_dt*(b_P - d_P - a_P*P_theo_past - c_P*N_theo_past))
                
                t = t + Euler_dt

                if t >= nextrecordtime:
#                    print('EUler', t)
                    history_cells_theo.append([t,N_theo_now,P_theo_now])
                    nextrecordtime += timelapse_Euler
            
    
            history_Euler = np.array(history_cells_theo)
            # Arrays that contain numbers of cells at each time step in Euler method
            history_theo_N = history_Euler[:,1]
            history_theo_P = history_Euler[:,2]
            history_theo_t = history_Euler[:,0]
            print('Euler time', history_theo_t)
            
        else:
            history_ensemble_N = np.vstack((history_ensemble_N,history[:,1]))
            history_ensemble_P = np.vstack((history_ensemble_P,history[:,2]))


    average_history_N = np.mean(history_ensemble_N,axis = 0)
    deviation_history_N = np.std(history_ensemble_N,axis = 0)
    
    average_history_P = np.mean(history_ensemble_P,axis = 0)
    deviation_history_P = np.std(history_ensemble_P,axis = 0)
    
    

    plt.tight_layout()
    
    # Experimental plot
    for row in history_ensemble_N:
        plt.plot(time,row, color = colorblind[colorid] , linestyle ='-',alpha = 0.1)  #plots each N(t) for each experiment

 
    plt.plot(time,average_history_N,color = bright[colorid] , linestyle ='-',lw = 2, label='Stochastic model N')  #plots the average N(t) for all experiments
    
    
    print('deviation_history_N', deviation_history_N)
    print('average_history_N', average_history_N)
    
    print('suma', average_history_N + deviation_history_N)

    
    plt.fill_between(time, average_history_N - deviation_history_N, average_history_N + deviation_history_N,  color = pastel[colorid], alpha = 0.75)
    
    
    
    # Theoretical plot (Euler)
    plt.plot(history_theo_t, history_theo_N, linestyle='--', color=dark[colorid], lw = 1.5, label='Deterministic model N')


    if P0>0:
        for row in history_ensemble_P:
            plt.plot(time,row, color = colorblind[colorid_P] , linestyle ='-',alpha = 0.1)   #plots each P(t) for each experiment
        plt.plot(time,average_history_P,color = bright[colorid_P] , linestyle ='-' ,lw = 2, label='Stochastic model P')  #plots the average P(t) for all experiments
        plt.fill_between(time, average_history_P - deviation_history_P, average_history_P + deviation_history_P, color = pastel[colorid_P], alpha = 0.75 )
        plt.plot(history_theo_t, history_theo_P, linestyle='--', color=dark[colorid_P], lw = 1.5, label='Deterministic model P')  

#    ax = plt.gca()
#    ax.set_yscale('log')

    plt.xlabel('Time $t$')
    plt.ylabel('Number of cells $N(t)$')
    plt.legend(loc='lower right')
    
    if hold is False:
        plt.savefig('two_species_MEAN_FIELD_D={:.3f}_b_={:.4f}_d_={:.4f}_a_={:.4f}_cN_={}_cP_={}_l_={}.pdf'.format(D,b_N,d_N,a_N, c_N, c_P, l_N), dpi=350)
        plt.show()

    
    

##Run the ensemble simulation
    
# PLOT 1
        
#print('plot 1')
#
#b_N = 0.055
#two.b_N = b_N
#
#d_N =0.015
#two.d_N = d_N 
#
#a_N = 0.00030
#two.a_N = a_N
#
        
        
#l_N = 25
#two.l_N = l_N
##
#        
#l_P = 25
#two.l_P = l_P
#
#D = 0.001
#two.D =  D 
#                
#run_ensemble_two_species(hold=False, colorid = 0)

#
## PLOT 2
#
#print('plot 2')


#b_N = 0.055
#two.b_N = b_N
#
#d_N =0.015
#two.d_N = d_N 
#
#a_N = 0.00030
#two.a_N = a_N
#
        
#l_N = 25
#two.l_N = l_N
#        
#l_P = 25
#two.l_P = l_P
#
#D = 1.000
#two.D =  D 
#
#run_ensemble_two_species(hold=False, colorid = 0)

#
## PLOT 3

#print('plot 3')
##
#b_N = 0.055
#two.b_N = b_N
##
#d_N =0.015
#two.d_N = d_N 

#a_N = 0.00030
#two.a_N = a_N
##
#l_N = 25
#two.l_N = l_N
#
#l_P = 25
#two.l_P = l_P
#
#D = 10.000
#two.D =  D 
#
#run_ensemble_two_species(hold=False, colorid = 0)





