# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 20:02:31 2019

@author: casmp
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import cells_comp_interactive as comp
import os as os
from scipy.interpolate import interp1d
from scipy import stats
import seaborn as sns

   
#Time parameters
dt = comp.dt
timelapse = comp.timelapse # how often to record   #before: 100
Tfinal = comp.Tfinal  #Before: 3000
n = comp.n  #time steps

#Spatial parameters
L = comp.L #length of box

#Cell parameters  
N0 = comp.N0 #int(input('Introduce initial no. of cells:')) #no. of cells  
a = comp.a  #proliferation rate
d_0 = comp.d_0  #death rate
d_1 = comp.d_1
K = (a - d_0)/d_1
# K = 20 for a-d_0 = 0.0020 and d_1 = 10**(-4)


# colourcycle palettes
dark = sns.color_palette("dark")
bright = sns.color_palette("bright")
colorblind = sns.color_palette("colorblind")
deep = sns.color_palette("deep")
pastel = sns.color_palette("pastel")
    

def save_ensemble_data(D,l, repetitions=30):  

    for rep in range(repetitions):
        Path1 ='Files/One/History/history_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,rep,Tfinal,N0,a,d_0)  #history of cells data
        Path2 ='Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,rep,Tfinal, N0, a, d_0)   #final number of cells data
        
        Path3 ='Files/One/History/time_stride_D_{:.3f}_l_{:.3f}_rep{}_Tfinal{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,rep,Tfinal,N0,a,d_0)  #time vector for history of cells
        print('repetition', rep, Path1, Path2)  
        if (os.path.exists(Path1) is False) or (os.path.exists(Path2) is False):
            
            print('Repetition', rep)
            
            # Run experiment
            history,cells = comp.run_cells_linear(D,l)
            
            # Collect data from experiment
            history_N = history[:,1]
            Nfinal = np.array([history[-1,1]])  # CHANGE THIS - NEED 1D ARRAY, NOT 0D
            
            # Calculate stride to reduce length of history_ensemble
            stride = int(len(history_N)/100)
            
            if stride == 0:
                stride += 1
                
            # Save data for history_ensemble and Nfinal
            np.savetxt('Files/One/History/history_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,rep,Tfinal,N0,a,d_0), history_N[::stride])
            np.savetxt('Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,rep,Tfinal, N0, a, d_0), Nfinal)
            
            if os.path.exists(Path3) == False:
                time = history[:,0]
                time_stride = time[::stride]
                np.savetxt('Files/One/History/time_stride_D_{:.3f}_l_{:.3f}_Tfinal_{}.dat'.format(D,l,Tfinal), time_stride)
            



def plot_D_Nfinal(fixedl,minD, maxD):
    
    list_final=[]
    N_aver=[]
    Nfinal_vector_hist = []
    
    for i, D in enumerate(np.geomspace(minD,maxD,15)):
        
        Path = 'Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,fixedl,0,Tfinal, N0, a, d_0)
        
        # Checking that we have a file for those values of D and l
        if os.path.exists(Path) == True:
            print('Reading file',Path)
            
            reps = 0
            finished_search = False
            Nfinal_vector = np.ones(0)
            while(finished_search is False):
                print('Loading repeat', reps)
                path_try = 'Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, a, d_0)
                if os.path.exists(path_try)==True and (reps<31):
                    Nfinal_vector = np.hstack((Nfinal_vector,np.loadtxt('Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,fixedl,reps,Tfinal, N0, a, d_0))))    
                    reps += 1
                else:
                    finished_search = True
        else:
            print('File ',Path, 'not found!')
                        
                
        # Calculates average Nfinal for each D
        average_Nfinal = np.mean(Nfinal_vector)
        std_Nfinal = np.std(Nfinal_vector) #standard deviation of Nfinal
        sem_Nfinal = stats.sem(Nfinal_vector)  #standard error of the mean
        N_aver.append(average_Nfinal)
        
        Nfinal_vector_hist = np.hstack((Nfinal_vector_hist, Nfinal_vector))
            
            
        list_final.append({'D':D,'Nfinal':Nfinal_vector,
                           'Nfinal_aver':average_Nfinal,'Nfinal_std':std_Nfinal, 'Nfinal_sem':sem_Nfinal})
            
    print('list_final is', list_final)
    
    
    plt.figure()
    plt.tight_layout()
    
    D_array = []
    N_aver_array = []
    N_std_array = []
    N_sem_array = []
    
    for element in list_final:
        D_array.append(element['D'])
        N_aver_array.append(element['Nfinal_aver'])
        N_std_array.append(element['Nfinal_std'])
        N_sem_array.append(element['Nfinal_sem'])
    
    D_array = np.array(D_array)
    N_aver_array = np.array(N_aver_array)
    N_std_array = np.array(N_std_array)
    N_sem_array = np.array(N_sem_array)

    plt.errorbar(D_array,N_aver_array,N_std_array, capsize=3, alpha = 0.3, ecolor= pastel[0])
    
    plt.plot(D_array, N_aver_array, marker='.', color=bright[0], alpha = 1, label='Experimental values')
    
    plt.fill_between(D_array, N_aver_array - N_sem_array, N_aver_array + N_sem_array,  color = pastel[0] , alpha = 0.75)
    
    
    plt.title('$\\ell$ = {}'.format(fixedl))
    plt.legend(loc='lower right')
    
    plt.xlabel('Diffusion ($D$)')
    plt.ylabel('Average final number of cells ($\\langle N_{final}\\rangle$)')
    
    ax = plt.gca()
#    xleft, xright = ax.get_xlim()
#    yleft, yright = ax.get_ylim()
#    aspectratio = 1
#    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    
    ax.set_xscale('log')
    
    plt.savefig('Graphs\\One\\D_vs_l\\Nfinal vs D with fixed l={}.pdf'.format(fixedl), dpi=350)
    plt.show()
    

    
    
    
    

def plot_l_Nfinal(fixedD,minl, maxl):
    
    list_final=[]
    N_aver = []
    Nfinal_vector_hist = []
    
    for i, l in enumerate(np.linspace(minl,maxl, 15, endpoint=True)):
        
        Path = 'Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(fixedD,l,0,Tfinal, N0, a, d_0)
        
        # Checking that we have a file for those values of D and l
        if os.path.exists(Path) == True:
            print('Reading file',Path)
            
            reps = 0
            finished_search = False
            Nfinal_vector = np.ones(0)
            while(finished_search is False):
                print('Loading repeat', reps)
                path_try = 'Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, a, d_0)
                if (os.path.exists(path_try)==True) and (reps<31):
                    Nfinal_vector = np.hstack((Nfinal_vector,np.loadtxt('Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(fixedD,l,reps,Tfinal, N0, a, d_0))))    
                    reps += 1
                else:
                    finished_search = True
        else:
            print('File ',Path, 'not found!')
                        
                
        # Calculates average Nfinal for each D
        average_Nfinal = np.mean(Nfinal_vector)
        std_Nfinal = np.std(Nfinal_vector)  #standard deviation of Nfinal
        sem_Nfinal = stats.sem(Nfinal_vector)  #standard error of the mean
        N_aver.append(average_Nfinal)
        
        
        Nfinal_vector_hist = np.hstack((Nfinal_vector_hist,Nfinal_vector))
            
            
        list_final.append({'l': l,'Nfinal':Nfinal_vector,
                           'Nfinal_aver':average_Nfinal,'Nfinal_std':std_Nfinal, 
                           'Nfinal_sem':sem_Nfinal})
            
    
    plt.figure()
    plt.tight_layout()
    
    l_array = []
    N_aver_array = []
    N_std_array = []
    N_sem_array = []
    
    for element in list_final:
        l_array.append(element['l'])
        N_aver_array.append(element['Nfinal_aver'])
        N_std_array.append(element['Nfinal_std'])
        N_sem_array.append(element['Nfinal_sem'])
    
    l_array = np.array(l_array)
    N_aver_array = np.array(N_aver_array)
    N_std_array = np.array(N_std_array)
    N_sem_array = np.array(N_sem_array)
    
    
    plt.errorbar(l_array,N_aver_array,N_std_array, capsize=3, alpha = 0.3, ecolor= pastel[0])
    
    plt.plot(l_array, N_aver_array, marker='.', color=bright[0], alpha = 1, label='Experimental values')
    
    plt.fill_between(l_array, N_aver_array - N_sem_array, N_aver_array + N_sem_array,  color = pastel[0] , alpha = 0.75)
    
    
    plt.title('$D$ = {}'.format(fixedD))
    plt.legend(loc='lower right')
        
    plt.xlabel('Interaction range ($\\ell$)')
    plt.ylabel('Average final number of cells ($\\langle N_{final}\\rangle$)')
    
#    ax = plt.gca()
#    xleft, xright = ax.get_xlim()
#    yleft, yright = ax.get_ylim()
#    aspectratio = 1
#    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    
    plt.savefig('Graphs\\One\\D_vs_l\\Nfinal vs l with fixed D={:.3f}.pdf'.format(fixedD), dpi=350)
    plt.show()


    
    
    
def histogram(D,l):
    
    Path = 'Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,0,Tfinal, N0, a, d_0)
        
        # Checking that we have a file for those values of D and l
    if os.path.exists(Path) == True:
        print('Reading file',Path)
            
        reps = 0
        finished_search = False
        Nfinal_vector = np.ones(0)
        while(finished_search is False):
            print('Loading repeat', reps)
            path_try = 'Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,reps,Tfinal, N0, a, d_0)
            if (os.path.exists(path_try)==True):
                Nfinal_vector = np.hstack((Nfinal_vector,np.loadtxt('Files/One/Nfinal/Nfinal_D_{:.3f}_l_{:.3f}_rep_{}_Tfinal_{}_N0_{}_a_{}_d_0_{}.dat'.format(D,l,reps,Tfinal, N0, a, d_0))))    
                reps += 1
            else:
                finished_search = True
    else:
        print('File ',Path, 'not found!')
        
    
    average_Nfinal = np.mean(Nfinal_vector)
    std_Nfinal = np.std(Nfinal_vector)  #standard deviation of Nfinal
    sem_Nfinal = stats.sem(Nfinal_vector)  #standard error of the mean
    
    plt.figure()
    plt.tight_layout()
    plt.hist(Nfinal_vector, bins=20, color=bright[0], density=False)
    
    
    plt.title('$D = {}$ and $\\ell = {}$'.format(D,l))
    plt.xlabel('$N_{final}$')
    plt.ylabel('Counts')
    
    ax = plt.gca()
    xleft, xright = ax.get_xlim()
    yleft, yright = ax.get_ylim()
    aspectratio = 1
    ax.set_aspect((xright-xleft)/(yright-yleft)*aspectratio)
    plt.savefig('Graphs\\One\\D_vs_l\\histogram_D={:.3f}_l={:.3f}_aver_={:.2f}_std={:.2f}.pdf'.format(D,l, average_Nfinal, std_Nfinal), dpi=350)
    plt.show()

    
        


# Saving data for D vs l plots

for i in tqdm(np.geomspace(0.001,100.000,15)):    #geometrical series spacing / log scale spacing
    save_ensemble_data(i,0.500)
    save_ensemble_data(i,25.000)



for j in tqdm(np.linspace(0.500,25.000,15, endpoint=True)):
    save_ensemble_data(0.000,j)
    save_ensemble_data(0.001, j)
    save_ensemble_data(0.010,j)
    save_ensemble_data(1.000,j)
    save_ensemble_data(10.000,j)
    save_ensemble_data(100.000,j)



# Histograms data

print('Saving data set 1')  
save_ensemble_data(0.010, 0.500, 250)

print('Saving data set 2')  
save_ensemble_data(10.000, 0.500, 250)

print('Saving data set 3')  
save_ensemble_data(0.010, 25.000, 250)

print('Saving data set 4')  
save_ensemble_data(10.000, 25.000, 250)



# Histograms plots

histogram(0.010,0.500)
histogram(10.000,0.500)
histogram(0.010,25.000)
histogram(10.000,25.000)

    

## Plots

plot_D_Nfinal(0.500,0.001,100.000)
plot_D_Nfinal(25.000,0.001,100.000)

plot_l_Nfinal(0.000, 0.500, 25.000)
plot_l_Nfinal(0.001, 0.500, 25.000)
plot_l_Nfinal(1.000, 0.500, 25.000)
plot_l_Nfinal(10.000, 0.500, 25.000)
plot_l_Nfinal(100.000, 0.500, 25.000)


