# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 10:48:12 2019

@author: casmp
"""


import numpy as np
from random import choices

#This script contains the functions regarding two species cell competition


#Time variables   
dt = 0.025
timelapse = 1 # how often to record
Tfinal = 202
n = int(Tfinal/dt)  #timesteps


#Spatial variables
L = 50 #length of box
l_N = 0.5  #interaction range for species N
l_P = 20  #interaction range for species N
D = 1 # diffusion coefficient
diam = 0.1 # distance from each other at which cells divide


#Initial number of cells for each species (N and P)   
N0 = 5
P0 = 5
    
# Equations:
# dN / dt = b_N * N - (d_N + a_N * N  + c_N * P) * N
# dP / dt = b_P * P - (d_P + a_P * P + c_P * N) * P

#Proliferation rates
b_N = 0.070  
b_P = 0.070

#Death rates
d_N = 0.00005 #for species N
d_P = 0.00005  #for species P


#Coupling constans

#N
a_N= 0.00080 #with N
c_N= 0.0010 #with P

# P
a_P = 0.00080  #with P
c_P= 0.00060  #with N

# a_x -> competition with its own species x
# c_x -> competition of x with the other species y



def cells_evolution(cellNumber, cellarray, a, d):  #kills or gives birth to cells and moves them around
    
    if cellNumber > 0:
        u = np.random.rand(cellNumber)   #proliferation die thrown cellNumber times
        v = np.random.rand(cellNumber)   #death die thrown cellNumber times
        
        proliferatingCells = u < a*dt  # a*dt = probability of a cell proliferating in that time window
        survivingCells = v > d*dt  # b*dt = probability of a cell dying in dt
        
        new_d = d
        
        # Limiting events to one event per dt (either one cell dies or one cell proliferates)
        
        if (np.sum(proliferatingCells)+np.sum(np.logical_not(survivingCells)) > 0):
            
            prob_prol = np.sum(proliferatingCells)/(np.sum(proliferatingCells)+np.sum(np.logical_not(survivingCells)))
            
            if prob_prol < np.random.rand(1):
                proliferatingCells = np.zeros(cellNumber, dtype=bool)  #no cells proliferating
                dyingCell = choices(population=np.arange(cellNumber), k=1, weights=np.logical_not(survivingCells)/np.sum(np.logical_not(survivingCells)))  #cel that has been randomly chosen to die
                survivingCells = np.ones(cellNumber, dtype=bool)
                survivingCells [dyingCell[0]] = False
            
            else:
                survivingCells = np.ones(cellNumber, dtype=bool)
                prolCell = choices(population=np.arange(cellNumber), k=1, weights=proliferatingCells/np.sum(proliferatingCells))
                proliferatingCells = np.zeros(cellNumber, dtype=bool)
                proliferatingCells[prolCell[0]] = True
        
        
        
        # newcells_0 is the new cells that will be added to the system
        newcells_0= cellarray[proliferatingCells]
        newcells_d = new_d[proliferatingCells]
        cellNumber += np.sum(proliferatingCells)
        
        
        # If a proliferation event occurs, we add the daughter cell to the system at an angle theta from its mother cell
        if np.sum(proliferatingCells) > 0:
            theta = np.random.rand(np.sum(proliferatingCells))*2*np.pi
            newcells_x = newcells_0[:,0]+diam*np.cos(theta)
            newcells_y = newcells_0[:,1]+diam*np.sin(theta)
                    
            newcells=[]
            for i, cellx in enumerate(newcells_x):
                newcells.append([newcells_x[i], newcells_y[i]])           
            newcells_0 = newcells
        
    
        # removing dying cells
        cellarray = cellarray[survivingCells]
        new_d = new_d[survivingCells]
        cellNumber -= np.sum(np.logical_not(survivingCells))
        
        # adding newcells
        cellarray = np.vstack((cellarray,newcells_0))
        new_d = np.hstack((new_d,newcells_d))
        
        # moving cells
        cellarray[:,0] = cellarray[:,0] + np.sqrt(D*dt)*np.random.normal(size=cellNumber) # diffusion x
        cellarray[:,1] = cellarray[:,1] + np.sqrt(D*dt)*np.random.normal(size=cellNumber) # diffusion y
    
       
        
        # Back to the box (PBCs)
        
        # cell exits the box from the left
        exitBoxLeft = cellarray[:,0]<0
        mLeft = abs(cellarray[exitBoxLeft,0]/L)
        cellarray[exitBoxLeft,0] += np.ceil(mLeft)*L
                
        # cell exits the box from the right
        exitBoxRight = cellarray[:,0]>L
        mRight = abs(cellarray[exitBoxRight,0]/L)
        cellarray[exitBoxRight,0] -= np.floor(mRight)*L
        
        # cell exits the box from the bottom
        exitBoxBottom = cellarray[:,1]<0
        mBottom = abs(cellarray[exitBoxBottom,1]/L)
        cellarray[exitBoxBottom,1] += np.ceil(mBottom)*L
            
        # cell exits the box from the top
        exitBoxTop = cellarray[:,1]>L
        mTop = abs(cellarray[exitBoxTop,1]/L)
        cellarray[exitBoxTop,1] -= np.floor(mTop)*L
        
        return(cellNumber, cellarray, new_d)
        
    else: 
        # i.e. if cellNumber == 0
        return(0,np.zeros(0),np.zeros(0))
    


def run_cells_linear(D,l_N, l_P, interaction="all_plate"):
    
    # Initialitation of the variables
    t = 0
    history_cells = []  #N(t) & P(t)
    initcelllistN = [] # list of cells N
    initcelllistP = [] # list of cells P    
    nextrecordtime = 0 # next time to record time

    # starting the population
    
    # The information of the species N will be stored as an array with two columns, cellarrayN.
    # Each row is a N cell and the two columns are its x and y coordinates, respectively
    
    for k in range(N0):
        initcelllistN.append([np.random.rand()*L,np.random.rand()*L])
    cellarrayN = np.array(initcelllistN) 
    cellNumberN = N0 # This variable will count the number of N cells at each time point
    
    # Similarly, for P.
    for k in range(P0):
            initcelllistP.append([np.random.rand()*L,np.random.rand()*L])
    cellarrayP = np.array(initcelllistP) 
    cellNumberP = P0 # This variable will count the number of P cells at each time point

    # Loop in time
    for i in range(n):
        
        # SHORT-RANGE SIGNALLING
        
        if (interaction == "short_range"):
            
            #Death coefficient for each cell depending on cell density every 10 iterations
            if i%10==0:
                
                cellNumberN_N_l = np.array(np.zeros(cellNumberN))  #number of cells of species N within range l from cells of species N
                cellNumberP_P_l = np.array(np.zeros(cellNumberP))  #number of cells of species P within range l from cells of species P
                cellNumberN_P_l = np.array(np.zeros(cellNumberN))  #number of cells of species P within range l from cells of species N
                cellNumberP_N_l = np.array(np.zeros(cellNumberP))  #number of cells of species N within range l from cells of species P 
                
                #Calculate density of cells within l for each individual of species N and each death rate
                for icell, celli in enumerate(cellarrayN):
                    
                    for jcell, cellj in enumerate(cellarrayN):
                        if (jcell > icell):                     
                            dx_N = min(abs(cellj[0]-celli[0]), L - abs(cellj[0]-celli[0]))
                            dy_N = min(abs(cellj[1]-celli[1]), L - abs(cellj[1]-celli[1])) 
                            dist_N_N = (dx_N**2 + dy_N**2)**0.5   #distance to each N cell
                            
                            if dist_N_N < l_N:
                                cellNumberN_N_l[icell]+= 1   #number of N cells within range l surrounding N cell i
                                cellNumberN_N_l[jcell]+= 1
                           
                #Density of cells: P vs P
                for icell, celli in enumerate(cellarrayP):
                    
                    for jcell, cellj in enumerate(cellarrayP):
                        if (jcell > icell):             
                            dx_P = min(abs(cellj[0]-celli[0]), L - abs(cellj[0]-celli[0]))
                            dy_P = min(abs(cellj[1]-celli[1]), L - abs(cellj[1]-celli[1])) 
                            dist_P_P = (dx_P**2 + dy_P**2)**0.5
                            
                            if dist_P_P < l_P:
                                cellNumberP_P_l[icell]+= 1 #number of P cells within range l surrounding P cell i
                                cellNumberP_P_l[jcell]+= 1
                            
                # Density of cells: N vs P and P vs N
                for icell, celli in enumerate(cellarrayN):
                    
                    for jcell, cellj in enumerate(cellarrayP):
                            
                        dx_N_P = min(abs(cellj[0]-celli[0]), L - abs(cellj[0]-celli[0]))
                        dy_N_P = min(abs(cellj[1]-celli[1]), L - abs(cellj[1]-celli[1])) 
                        dist_N_P = (dx_N_P**2 + dy_N_P**2)**0.5  #distance to each P cell
                                
                        if dist_N_P < l_N:    
                            cellNumberN_P_l[icell]+= 1  #number of P cells around cell number i of N species
                        
                        if dist_N_P < l_P:
                            cellNumberP_N_l[jcell]+= 1  #number of N cells around cell number j of P species
                    
                
                #Calculating death rate vector (each effective apoptosis rate) for each species
                dcell_N = d_N + a_N*(cellNumberN_N_l/(np.pi*l_N**2))*(L**2) + c_N*(cellNumberN_P_l/(np.pi*l_N**2))*(L**2)
                dcell_P = d_P + a_P*(cellNumberP_P_l/(np.pi*l_P**2))*(L**2) + c_P*(cellNumberP_N_l/(np.pi*l_P**2))*(L**2) 
                
                
                #calculate surviving and dying cells and move surviving cells around
                cellNumberN, cellarrayN, dcell_N = cells_evolution(cellNumberN, cellarrayN, b_N, dcell_N)
                cellNumberP, cellarrayP, dcell_P = cells_evolution(cellNumberP, cellarrayP, b_P, dcell_P)
                
            else:
                  
                #calculate surviving and dying cells and move surviving cells around
                cellNumberN, cellarrayN, dcell_N = cells_evolution(cellNumberN, cellarrayN, b_N, dcell_N)
                cellNumberP, cellarrayP, dcell_P = cells_evolution(cellNumberP, cellarrayP, b_P, dcell_P)
        
        
        # LONG-RANGE SINGALLING 
        
        elif (interaction == "all_plate"):
            
            dcomp_N = d_N + a_N*cellNumberN + c_N*cellNumberP
            dcomp_P = d_P + a_P*cellNumberP + c_P*cellNumberN
            
            dcomp_N = np.ones(cellNumberN)*dcomp_N
            dcomp_P = np.ones(cellNumberP)*dcomp_P
                
            #calculate surviving and dying cells and move surviving cells around
            cellNumberN, cellarrayN, dcomp_N = cells_evolution(cellNumberN, cellarrayN, b_N, dcomp_N)
            cellNumberP, cellarrayP, dcomp_P = cells_evolution(cellNumberP, cellarrayP, b_P, dcomp_P)
            

        
        # updating time and saving N(t) if necessary
        t = t + dt

        if t >= nextrecordtime:
            # We record the information about the system after a specific timelapse
            history_cells.append([t,cellNumberN,cellNumberP])
            nextrecordtime += timelapse
        

    return np.array(history_cells), cellarrayN, cellarrayP