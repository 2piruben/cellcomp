# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 12:08:50 2019

@author: casmp
"""



import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from random import choices
import seaborn as sns
import cv2


        
dt = 0.5
timelapse = 1 # how often to record   #before: 100
Tfinal = 1000  #Before: 3000
n = int(Tfinal/dt)  #time steps
L = 50 #length of box


N0 = 3 #int(input('Introduce initial no. of cells:')) #no. of cells
    
a = 0.0050  #proliferation rate
d_0 = 0.0010  #death rate
d_1 = 10**(-4)
K = (a - d_0)/d_1
# K = 200 for a-d_0 = 0.0020 and d_1 = 10**(-5) 
# K = 40 for simulation -> a = 0.0050 , d_0 = 0.0010,d_1 = 10**(-4)

D = 0.25 # diffusion coefficient

# colour palettes
sbcolorcycle_dark = sns.color_palette("dark")
sbcolorcycle_light = sns.color_palette("bright")




          
def run_cells_linear(one_event = True, simulation=True):

    # Initialitation of the variables
    t = 0
    history_cells = []  #N(t)
    initcelllist = [] # list of cells    
    nextrecordtime = 0 # next time to record time
    
    if simulation==True:
        nim = 0 
        framenamelist = []  
        fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
        diam = 2  #diameter of cells for the simulation

    # starting population
    for k in range(N0):
        initcelllist.append([np.random.rand()*L,np.random.rand()*L])
    cellarray = np.array(initcelllist) # the information of the system will be stored as an array with two columns.
    # Each row is a cell and the two columns are position x and y
    cellNumber = N0 # this variable will count the number of cells at each time point

    # loop in time
    for i in range(n):
        
        #Now d depends on N(t) i.e. d(t)=f(N(t))
        d = d_0 + cellNumber*d_1

        u = np.random.rand(cellNumber)   #proliferation "dice" thrown cellNumber times
        v = np.random.rand(cellNumber)   #death "dice" thrown cellNumber times

        proliferatingCells = u < a*dt  # a*dt = probability of a cell proliferating in that time window dt aka prol. coeff.

        survivingCells = v > d*dt  # b*dt = probability of a cell dying in dt aka death coeff.
        
        
        # Limiting events to one event per dt (either one cell dies or one cell proliferates)
        if one_event==True:
            if (np.sum(proliferatingCells)+np.sum(np.logical_not(survivingCells)) > 0):
                
                prob_prol = np.sum(proliferatingCells)/(np.sum(proliferatingCells)+np.sum(np.logical_not(survivingCells)))
                
                if prob_prol < np.random.rand(1):
                    proliferatingCells = np.zeros(cellNumber, dtype=bool)  #no cells proliferating
                    dyingCell = choices(population=np.arange(cellNumber), k=1, weights=np.logical_not(survivingCells)/np.sum(np.logical_not(survivingCells)))  #cel that has been randomly chosen to die
                    survivingCells = np.ones(cellNumber, dtype=bool)
                    survivingCells [dyingCell[0]] = False
        #            print('Killing cell', dyingCell)
                
                else:
                    survivingCells = np.ones(cellNumber, dtype=bool)
                    prolCell = choices(population=np.arange(cellNumber), k=1, weights=proliferatingCells/np.sum(proliferatingCells))
                    proliferatingCells = np.zeros(cellNumber, dtype=bool)
                    proliferatingCells[prolCell[0]] = True
        #            print('Dividing cell', prolCell)
        #            print('Proliferating', proliferatingCells)
         
        
        # newcells_0 is the new cells that will be added to the system
        newcells_0 = cellarray[proliferatingCells]
        
        if np.sum(proliferatingCells) > 0:
            theta = np.random.rand(np.sum(proliferatingCells))*2*np.pi
            newcells_x = newcells_0[:,0]+diam*np.cos(theta)
#            print('cells_x', newcells_x)
            newcells_y = newcells_0[:,1]+diam*np.sin(theta)
#            print('cells_y', newcells_y)
            
            newcells=[]
            for i, cellx in enumerate(newcells_x):
                newcells.append([newcells_x[i], newcells_y[i]])           
            newcells_0 = newcells
#            print(newcells)
        
        
        cellNumber += np.sum(proliferatingCells)

        # removing dying cells
        cellarray = cellarray[survivingCells]
        cellNumber -= np.sum(np.logical_not(survivingCells))

        # adding newcells
        cellarray = np.vstack((cellarray,newcells_0))
#        cellNumber = len(cellarray)

#        #### adding cells by hand when 
#        if cellNumber == 0:
#            cellarray = np.array([[np.random.rand()*L,np.random.rand()*L]])
#            cellNumber = 1
            
        # moving cells 
        cellarray[:,0] = cellarray[:,0] + np.sqrt(2*D*dt)*np.random.normal(size=cellNumber) # diffusion x
        cellarray[:,1] = cellarray[:,1] + np.sqrt(2*D*dt)*np.random.normal(size=cellNumber) # diffusion y
        
        
        #  back to the box (periodic boundary conditions)   
        
        # cell exits the box from the left
        exitBoxLeft = cellarray[:,0]<0
        mLeft = abs(cellarray[exitBoxLeft,0]/L)
        cellarray[exitBoxLeft,0] += np.ceil(mLeft)*L
            
        # cell exits the box from the right
        exitBoxRight = cellarray[:,0]>L
        mRight = abs(cellarray[exitBoxRight,0]/L)
        cellarray[exitBoxRight,0] -= np.floor(mRight)*L
    
        # cell exits the box from the bottom
        exitBoxBottom = cellarray[:,1]<0
        mBottom = abs(cellarray[exitBoxBottom,1]/L)
        cellarray[exitBoxBottom,1] += np.ceil(mBottom)*L
        
        # cell exits the box from the top
        exitBoxTop = cellarray[:,1]>L
        mTop = abs(cellarray[exitBoxTop,1]/L)
        cellarray[exitBoxTop,1] -= np.floor(mTop)*L


#        print('cell array AFTER', cellarray, 'at time t=', t)
        
        # updating time and saving N(t) if necessary
        t = t + dt
        

        if t >= nextrecordtime:
            history_cells.append([t,cellNumber])

            if simulation == True:
                print('creating snapshot number', nim)
                print('cell array at nim=', nim, 'is', cellarray)
                plt.scatter(cellarray[:,0], cellarray[:,1], 
                                marker='.', s=(L/6)**2,
                                color= sbcolorcycle_light[0], edgecolors= sbcolorcycle_dark[0])
                
                ax.set_aspect('equal', 'box')
                ax.set_xlim(0,L)
                ax.set_ylim(0,L)
                plt.tight_layout()
                plt.savefig("Simulations/One/Comp_no_interaction/comp_no_int{}.png".format(nim),dpi=350)
                plt.savefig("Simulations/One/Comp_no_interaction/comp_no_int{}.pdf".format(nim),dpi=350)
                framenamelist.append("Simulations/One/Comp_no_interaction/comp_no_int{}.png".format(nim))
                plt.cla()
                
                nim += 1
            
#            print('Cellarray', cellarray, 'at time', t)
            
            nextrecordtime += timelapse

    return framenamelist, np.array(history_cells), cellarray

def run_ensemble():

    repetitions = 50
    history_ensemble = np.ones(0) # empty numpy array
    for rep in tqdm(range(repetitions)):
        history,cells = run_cells_linear()
        if rep==0:
            history_ensemble = history[:,1]
        else:
            history_ensemble = np.vstack((history_ensemble,history[:,1]))


    average_history = np.mean(history_ensemble,axis = 0)
    time = history[:,0] # time vector

    for row in history_ensemble:
        plt.plot(time,row,'k-',alpha = 0.2)
    plt.plot(time,average_history,'b',lw = 2)

    # theoretical curve
    #Ntheo = N0*np.exp((a-d_0)/dt*time)
    Ntheo = ((((1/N0)-(1/K))*(np.exp((d_0-a)*time)))+(1/K))**(-1)
    
    plt.plot(time,Ntheo,'r',lw = 2)
    plt.plot(time,K*np.ones_like(time),'g',lw = 2)
    plt.xlabel('time')
    plt.ylabel('Number of cells')
    plt.savefig('logistic_growth.png')
    plt.show()

# Run the ensemble simulation
#run_ensemble()
    
# Create video for simulation

framenamelist, history, c_array = run_cells_linear()

frame0 = cv2.imread(framenamelist[0])
height, width, layers = frame0.shape
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
video = cv2.VideoWriter('Simulations/One/Comp_no_interaction/cell_logistic_mean_field.mp4',fourcc, 10,(width,height))

for frame in framenamelist:
    imag = cv2.imread(frame)
    video.write(imag)
    
cv2.destroyAllWindows()
video.release()