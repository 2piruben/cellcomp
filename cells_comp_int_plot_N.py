# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 15:14:20 2019

@author: casmp
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import cells_comp_interactive as comp
import seaborn as sns


#Time parameters
dt = comp.dt
timelapse = comp.timelapse # how often to record   #before: 100
Tfinal = comp.Tfinal  #Before: 3000
n = comp.n  #time steps

#Spatial parameters
L = comp.L #length of box
l = comp.l
D = comp.D # diffusion coefficient

#Cell parameters  
N0 = comp.N0 #int(input('Introduce initial no. of cells:')) #no. of cells  
a = comp.a  #proliferation rate
d_0 = comp.d_0 #death rate
d_1 = comp.d_1
K = (a - d_0)/d_1
# K = 20 for a-d_0 = 0.0020 and d_1 = 10**(-4) 

# colourcycle palettes
dark = sns.color_palette("dark")
bright = sns.color_palette("bright")
colorblind = sns.color_palette("colorblind")
deep = sns.color_palette("deep")
pastel = sns.color_palette("pastel")

def run_ensemble():

    repetitions = 100
    history_ensemble = np.ones(0) # empty numpy array
    for rep in tqdm(range(repetitions)):
        history,cells = comp.run_cells_linear(D,l)
        print(history)
        print(history[:,1])
        if rep==0:
            history_ensemble = history[:,1]
        else:
            history_ensemble = np.vstack((history_ensemble,history[:,1]))
        print(history_ensemble)


    average_history = np.mean(history_ensemble,axis = 0)
    time = history[:,0] # time vector

    for row in history_ensemble:
        plt.plot(time,row,color = colorblind[9] , linestyle ='-',alpha = 0.1)  #plots each experiment
        
    plt.plot(time,average_history,'b',lw = 2)  #plots the average N(t) for all experiments

    # theoretical curve
    #Ntheo = N0*np.exp((a-d_0)/dt*time)
    Ntheo = ((((1/N0)-(1/K))*(np.exp((d_0-a)*time)))+(1/K))**(-1)
    
    plt.plot(time,Ntheo,'r',lw = 2)  #plots theoretical prediction
    plt.plot(time,K*np.ones_like(time),'g',lw = 2)  #plots N (t) = K
    plt.xlabel('time')
    plt.ylabel('Number of cells')
    plt.savefig('cells_growth_interactive.png')
    plt.show()

#Run the ensemble simulation
run_ensemble()