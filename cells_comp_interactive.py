# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 17:18:02 2019

@author: casmp
"""


import numpy as np
from random import choices

#This script contains the functions regarding interactive cell competition

          
#Time parameters
dt = 0.5
timelapse = 200 # how often to record   #before: 100
Tfinal = 5000  #Before: 3000
n = int(Tfinal/dt)  #time steps

#Spatial parameters
L = 50 #length of box
l = 0.5
D = 0 # diffusion coefficient
diam = 0.1 # distance from each other at which cells divide

#Cell parameters  
N0 = 30 #int(input('Introduce initial no. of cells:')) #no. of cells  
a = 0.0030  #proliferation rate
d_0 = 0.0008  #death rate
d_1 = 2*10**(-5)
K = (a - d_0)/d_1
# K = 20 for a-d_0 = 0.0020 and d_1 = 10**(-4) 

def cells_evolution(cellNumber, cellarray, a, d):  #kills or gives birth to cells and moves them around
    
    u = np.random.rand(cellNumber)   #proliferation "dice" thrown cellNumber times
    v = np.random.rand(cellNumber)   #death "dice" thrown cellNumber times
    
    proliferatingCells = u < a*dt  # a*dt = probability of a cell proliferating in that time window dt aka prol. coeff.
    survivingCells = v > d*dt  # d*dt = probability of a cell dying in dt aka death coeff.
  
    new_d = d
    
#    print('Old: ',cellNumber, len(cellarray))
    
    # Limiting events to one event per dt (either one cell dies or one cell proliferates)
    
    if (np.sum(proliferatingCells)+np.sum(np.logical_not(survivingCells)) > 0):
        
        prob_prol = np.sum(proliferatingCells)/(np.sum(proliferatingCells)+np.sum(np.logical_not(survivingCells)))
        
        if prob_prol < np.random.rand(1):
            proliferatingCells = np.zeros(cellNumber, dtype=bool)  #no cells proliferating
            dyingCell = choices(population=np.arange(cellNumber), k=1, weights=np.logical_not(survivingCells)/np.sum(np.logical_not(survivingCells)))  #cel that has been randomly chosen to die
            survivingCells = np.ones(cellNumber, dtype=bool)
            survivingCells [dyingCell[0]] = False
#            print('Killing cell', dyingCell)
        
        else:
            survivingCells = np.ones(cellNumber, dtype=bool)
            prolCell = choices(population=np.arange(cellNumber), k=1, weights=proliferatingCells/np.sum(proliferatingCells))
            proliferatingCells = np.zeros(cellNumber, dtype=bool)
            proliferatingCells[prolCell[0]] = True
#            print('Dividing cell', prolCell)
#            print('Proliferating', proliferatingCells)

        
#    newcells is the new cells that will be added to the system
    newcells_0 = cellarray[proliferatingCells]

    if np.sum(proliferatingCells) > 0:
        theta = np.random.rand(np.sum(proliferatingCells))*2*np.pi
        newcells_x = newcells_0[:,0]+diam*np.cos(theta)
#            print('cells_x', newcells_x)
        newcells_y = newcells_0[:,1]+diam*np.sin(theta)
#            print('cells_y', newcells_y)
            
        newcells=[]
        for i, cellx in enumerate(newcells_x):
            newcells.append([newcells_x[i], newcells_y[i]])           
        newcells_0 = newcells

    newcells_d = new_d[proliferatingCells]
    cellNumber += np.sum(proliferatingCells)
    
    
#    # rebound condition
#    if cellNumber == np.sum(np.logical_not(survivingCells)):
#        cellarray = np.vstack((cellarray,cellarray[-1,:]))
#        survivingCells = np.hstack((survivingCells,[True]))
#        cellNumber += 1
#        new_d = new_d[0]*np.ones(cellNumber)  #CHANGED


    # removing dying cells
    cellarray = cellarray[survivingCells]
    new_d = new_d[survivingCells]
    cellNumber -= np.sum(np.logical_not(survivingCells))
    
    
    # adding newcells
    cellarray = np.vstack((cellarray,newcells_0))
#    print('cellarray:', cellarray)
    new_d = np.hstack((new_d,newcells_d))
    
  
#    print('New: ',cellNumber,len(cellarray))
    
    # moving cells 
    cellarray[:,0] = cellarray[:,0] + np.sqrt(2*D*dt)*np.random.normal(size=cellNumber) # diffusion x
    cellarray[:,1] = cellarray[:,1] + np.sqrt(2*D*dt)*np.random.normal(size=cellNumber) # diffusion y

    # back to the box (periodic boundary conditions)
    
    # cell exits the box from the left
    exitBoxLeft = cellarray[:,0]<0
    mLeft = abs(cellarray[exitBoxLeft,0]/L)
    cellarray[exitBoxLeft,0] += np.ceil(mLeft)*L
            
    # cell exits the box from the right
    exitBoxRight = cellarray[:,0]>L
    mRight = abs(cellarray[exitBoxRight,0]/L)
    cellarray[exitBoxRight,0] -= np.floor(mRight)*L
    
    # cell exits the box from the bottom
    exitBoxBottom = cellarray[:,1]<0
    mBottom = abs(cellarray[exitBoxBottom,1]/L)
    cellarray[exitBoxBottom,1] += np.ceil(mBottom)*L
        
    # cell exits the box from the top
    exitBoxTop = cellarray[:,1]>L
    mTop = abs(cellarray[exitBoxTop,1]/L)
    cellarray[exitBoxTop,1] -= np.floor(mTop)*L
    
    
    return(cellNumber, cellarray, new_d)
    


def run_cells_linear(D,l, interaction='short_range'):

    # Initialitation of the variables
    t = 0
    history_cells = []  #N(t)
    initcelllist = [] # list of cells    
    nextrecordtime = 0 # next time to record time

    # starting population
    for k in range(N0):
        initcelllist.append([np.random.rand()*L,np.random.rand()*L])
    cellarray = np.array(initcelllist) # the information of the system will be stored as an array with two columns.
    # Each row is a cell and the two columns are position x and y
    cellNumber = N0 # this variable will count the number of cells at each time point

    # loop in time
    for i in range(n):
        
        if interaction == 'short_range':
            #death coefficient for each cell depending on cell density every 1 iterations
            if i%5==0:
#                print('Time step:', i)
                cellNumber_l = np.array(np.zeros(cellNumber))  #vector that stores the number of cells within range l from each cell
                
                for icell, celli in enumerate(cellarray):
                    
                    for jcell, cellj in enumerate(cellarray):
                        if (jcell > icell):
                            dx = min(abs(cellj[0]-celli[0]), L - abs(cellj[0]-celli[0]))
                            dy = min(abs(cellj[1]-celli[1]), L - abs(cellj[1]-celli[1])) 
                            dist = (dx**2 + dy**2)**0.5
                            
                            if dist < l:
                                cellNumber_l[icell]+= 1
                                cellNumber_l[jcell]+= 1
                                
                dcell = d_0 + (cellNumber_l/(np.pi*l**2))*(L**2)*d_1 #vector that stores the death rate for each cell, we have one death rate per cell depending on how crowded its surroundings are (density of cells)
            
            #calculate surviving and dying cells and move surviving cells around
            cellNumber, cellarray, dcell = cells_evolution(cellNumber,cellarray,a,dcell)
            
        if interaction == 'all_plate' :  
            
            #d depends on N(t) in all plate
            d = d_0 + cellNumber*d_1
            
            #calculate surviving and dying cells and move surviving cells around
            cellNumber, cellarray = cells_evolution(cellNumber,cellarray,a,d)
            
            
        # updating time and saving N(t) if necessary
        t = t + dt

        if t >= nextrecordtime:
            history_cells.append([t,cellNumber])
            nextrecordtime += timelapse
        

    return np.array(history_cells), cellarray